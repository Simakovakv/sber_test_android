package ru.home.simakova.testapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {
    private static final String VALUE = "value";
    private TextView mTextView2;
    private Button mButtonAbout;
    private Button mButtonStart;

    public static final Intent newIntent(Context context, String text) {
        Intent intent = new Intent(context, InfoActivity.class);
        intent.putExtra(VALUE, text);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        initViews();
        initListeners();
    }


    private void initViews(){
        mTextView2 = findViewById(R.id.textView2);
        mTextView2.setText(getIntent().getStringExtra(VALUE));
        mButtonAbout = findViewById(R.id.button3);
        mButtonStart = findViewById(R.id.button2);
    }

    private void initListeners(){
        mButtonAbout.setOnClickListener(new ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button3:
                    startActivity(DetailActivity.newIntent(InfoActivity.this, "message"));
                    break;
                case R.id.button2: setResult(RESULT_CANCELED);
                    finish();

            }
        }
    }

}
