package ru.home.simakova.testapp;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE = 1001;
    public static final String RESULT_VALUE = "result value";
    private TextView mTextView;
    private Button mButtonGo;
    private EditText mEditText2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        initListeners();
    }
    private void initViews(){
        mTextView = findViewById(R.id.textView);
        mButtonGo = findViewById(R.id.button);
        mEditText2 = findViewById(R.id.editText2);
    }
    private void initListeners(){
        mButtonGo.setOnClickListener(new ButtonClickListener());
    }
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button:
                    startActivity(InfoActivity.newIntent(MainActivity.this, mEditText2.getText().toString()));
                    break;
            }
        }
    }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            if(data!=null && requestCode == REQUEST_CODE){
                mEditText2.setText(data.getStringExtra(RESULT_VALUE));
            }
        }

}
