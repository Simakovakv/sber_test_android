package ru.home.simakova.testapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    private static final String VALUE = "value";
    public static final int REQUEST_CODE = 1001;
    public static final String RESULT_VALUE = "result value";
    private TextView mTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initViews();
    }
    private void initViews(){
        mTextView = findViewById(R.id.textView3);
        mTextView.setText(getIntent().getStringExtra(VALUE));
    }
    public static final Intent newIntent(Context context, String text) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(VALUE, text);
        return intent;
    }
        @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(DetailActivity.RESULT_VALUE, mTextView.getText().toString() + " " + Long.toString(System.currentTimeMillis()));
        setResult(MainActivity.REQUEST_CODE, intent);
    }
}
